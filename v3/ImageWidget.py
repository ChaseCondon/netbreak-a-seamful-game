import copy
import torch
import torch.nn.functional  as F
import numpy                as np

from kivy.app               import App
from Enums                  import Size
from kivy.uix.image         import Image
from math                   import floor
from kivy.graphics.texture  import Texture

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


class GameImage(Image):
    shown_img = None
    shown_arr = None
    real_arr = None
    tensor = None
    last_arr = None
    last_real = None
    last_tensor = None
    net = None
    label = None

    rgb = [255, 0, 0]
    brush_size = Size.SMALL
    remaining = None

    def retrieve_state(self):
        return self.tensor

    def get_labels(self):
        return self.label

    def get_arr(self):
        return self.real_arr

    def init_game(self, imgs, img, net):
        self.net = net
        self.reset_image(imgs, img)
        App.get_running_app().time = 0
        App.get_running_app().ready=True

    def reset_image(self, imgs, img):
        self.remaining = 20
        App.get_running_app().remaining = "Remaining pixels\n" + str(self.remaining)
        self.changes = list()

        self.tensor = copy.deepcopy(imgs)

        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.real_arr = (img*255).astype("uint8")
        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img

        self.last_arr = None
        self.last_real = None
        self.last_tensor = None

        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            App.get_running_app().classification = "Classification:\n" + "\n".join(map(lambda a: "%s: %s"%(a[0], a[1]), probs_list))
    
    def change_color(self, value):
        self.rgb = value

    def change_brush_size(self, size):
        self.brush_size = Size(size)

    def undo_change(self):
        if self.last_arr is not None:
            self.shown_arr = copy.deepcopy(self.last_arr)
            self.real_arr = copy.deepcopy(self.last_real)
            self.last_tensor = copy.deepcopy(self.last_tensor)
            self.last_arr = None
            self.last_real = None
            self.last_tensor = None
            self.remaining += 1
            self.update_screen()

    def change_pixel(self, y, x):
        self.real_arr[y, x, :] = self.rgb

        self.tensor[:,0,y,x] = (self.rgb[0])/255
        self.tensor[:,1,y,x] = (self.rgb[1])/255
        self.tensor[:,2,y,x] = (self.rgb[2])/255

    def update_pixel(self, y, x):
        if self.real_arr[y,x,0] == self.rgb[0] and self.real_arr[y,x,1] == self.rgb[1] and self.real_arr[y,x,2] == self.rgb[2]:
            return
        if self.remaining is 0:
            return
        self.remaining -= 1

        self.last_arr = copy.deepcopy(self.shown_arr)
        self.last_real = copy.deepcopy(self.real_arr)
        self.last_tensor = copy.deepcopy(self.tensor)

        self.change_pixel(y, x)

        if self.brush_size is Size.MEDIUM or self.brush_size is Size.LARGE:
            if y is not 0:
                self.change_pixel(y-1, x)

            if y+1 is not 32:
                self.change_pixel(y+1, x)

            if x is not 0:
                self.change_pixel(y, x-1)

            if x+1 is not 32:
                self.change_pixel(y, x+1)

            if self.brush_size is Size.LARGE:
                if y is not 0 and x is not 0:
                    self.change_pixel(y-1, x-1)

                if y+1 is not 32 and x+1 is not 32:
                    self.change_pixel(y+1, x+1)

                if x is not 0 and y+1 is not 32:
                    self.change_pixel(y+1, x-1)

                if x+1 is not 32 and y is not 0:
                    self.change_pixel(y-1, x+1)

        self.update_screen()

    def update_screen(self):
        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img
        
        App.get_running_app().remaining = "Remaining pixels\n" + str(self.remaining)
        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            self.label = probs_list
            App.get_running_app().classification = "Classification:\n" + "\n".join(map(lambda a: "%s: %s"%(a[0], a[1]), probs_list))
    
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            # Account for screen scaling
            # Image will always keep resolution of multiples of 32x32
            # Therefor the scale of the image is the minimum of the x and y scalars
            x_scale = self.width / 32
            y_scale = self.height / 32
            true_scale = min(x_scale, y_scale)
            if true_scale == x_scale:
                true_x = self.x
                true_y = self.y + (self.height-true_scale*32)/2
            else:
                true_y = self.y
                true_x = self.x + (self.width-true_scale*32)/2

            # The collide point for the widget isn't the collide point for the image
            # when the x and y scalars aren't equal. This takes into account the true
            # scale and image's true x and y
            if touch.x >= true_x and touch.x <= true_x + 32*true_scale and \
               touch.y >= true_y and touch.y <= true_y + 32*true_scale:
                y = floor((32*true_scale-(touch.y - true_y))/true_scale)
                x = floor((touch.x - true_x)/true_scale)
            
                self.update_pixel(y, x)
