import pickle
import numpy                as np

from kivy.app               import App
from math                   import floor
from kivy.uix.popup         import Popup
from kivy.uix.label         import Label
from kivy.uix.button        import Button
from kivy.uix.boxlayout     import BoxLayout
from kivy.properties        import StringProperty
from kivy.graphics.texture  import Texture   
from kivy.core.window       import Window

Window.pos = (200, 200)
Window.size = (2400, 1200)


class ErrorPopup(Popup):
    pass


class AnalysisApp(App):
    file = []
    current_index = 0
    orig_class = StringProperty()
    new_class = StringProperty()
    numbers = StringProperty()
    orig_label = StringProperty()
    mod_label = StringProperty()
    time = StringProperty()
    total_score = StringProperty()
    total_time = StringProperty()

    def update_screen(self):
        current_round = self.file[self.current_index]

        self.orig_class = "Original\nClassification\n\n" + "\n".join(map(lambda a: "%s: %s"%(a[0], a[1]), current_round['original_outputs']))
        self.new_class = "Modified\nClassification\n\n" + "\n".join(map(lambda a: "%s: %s"%(a[0], a[1]), current_round['modified_outputs']))

        if current_round['original_class'] == current_round['modified_class']:
            color = '#FF0000'
        else:
            color = '#00FF00'

        self.orig_label = '[color=%s]Original Class:\n%s[/color]'%(color, current_round['original_class'])
        self.mod_label = '[color=%s]Modified Class:\n%s[/color]'%(color, current_round['modified_class'])
        minutes, seconds = divmod(current_round['time'], 60)
        self.time = (
            'Time\n%02d:%02d.[size=20]%02d[/size]' %
            (int(minutes), int(seconds),
            int(seconds * 100 % 100))
        )

        shown_orig_img = Texture.create(size=(32, 32), colorfmt="rgb")
        shown_orig = np.flipud(current_round['original_image'])
        shown_orig_img.blit_buffer(shown_orig.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.root.ids.original.texture = shown_orig_img

        shown_mod_img = Texture.create(size=(32, 32), colorfmt="rgb")
        shown_mod = np.flipud(current_round['modified_image'])
        shown_mod_img.blit_buffer(shown_mod.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.root.ids.modified.texture = shown_mod_img

        self.numbers = str(self.current_index+1) + ' / ' + str(len(self.file))

    def change_index(self, direction):
        if len(self.file) == 0:
            return
        if direction == -1 and self.current_index == 0:
            return
        if direction == 1 and self.current_index == len(self.file)-1:
            return
        self.current_index += direction
        self.update_screen()

    def open_file(self, path, selection):
        filepath = selection[0]
        self.root.current = 'view'

        print(filepath)
        print(filepath.split('.'))

        if len(filepath.split('.')) == 1 or 'pickle' not in filepath.split('.'):
            popup = ErrorPopup(title='Evaluation',
                      size_hint=(None, None), 
                      size=(1000, 400))
            popup.open()
            return
        with open(filepath, 'rb') as picklefile:
            self.file = pickle.load(picklefile)
        print("Game of length %d rounds loaded."%len(self.file))

        total_score = [0, 0]
        total_time = 0

        for i in range(len(self.file)):
            total_time += self.file[i]['time']
            if self.file[i]['original_class'] == self.file[i]['modified_class']:
                total_score[1] += 1
            else:
                total_score[0] += 1

        self.total_score = "Total Score\n%d / %d"%(total_score[0], total_score[1])

        minutes, seconds = divmod(total_time, 60)
        self.total_time = (
            'Total Time\n%02d:%02d.[size=20]%02d[/size]' %
            (int(minutes), int(seconds),
            int(seconds * 100 % 100))
        )

        self.update_screen()

    def _on_keyboard_down(self, instance, keyboard, keycode, text, modifiers):
        if keycode == 79:
            self.change_index(1)
        if keycode == 80:
            self.change_index(-1)

    def on_start(self):
        self.orig_class = 'Original\nClassification'
        self.new_class = "Modified\nClassification"
        self.numbers = '0 / 0'
        self.orig_label = 'Original Class:\n-'
        self.mod_label = "Modified Class:\n-"
        self.time = 'Time\n00:00.[size=20]00[/size]'
        self.total_time = 'Total Time\n00:00.[size=20]00[/size]'
        self.total_score = 'Score\n0 / 0'
        Window.bind(on_key_down=self._on_keyboard_down)

if __name__ == '__main__':
    AnalysisApp().run()