from enum import Enum


class Difficulty(Enum):
    EASY   = "Easy"
    MEDIUM = "Medium"
    HARD   = "Hard"


class Size(Enum):
    SMALL   = "Small"
    MEDIUM  = "Medium"
    LARGE   = "Large"


class Color(Enum):
    RED     = [255, 0, 0]
    YELLOW  = [255, 255, 0]
    BLUE    = [0, 0, 255]
    ORANGE  = [255, 128, 0]
    GREEN   = [0, 255, 0]
    PURPLE  = [255, 0, 255]