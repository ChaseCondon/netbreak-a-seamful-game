# Main imports
import os
import cv2
import torch
import shutil
import tkinter
import easygui
import tempfile
import torchvision
import numpy                  as np
import torch.nn               as nn
import torch.optim            as optim
import torchvision.transforms as transforms

from NeuralNet                import Net, train

# Global variables:
# ========================================
# Network and Classification Variables
trainloader = None
net = None
imgs = None
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# Game Variables
pixels_changed = 0
change_history = dict()
ix,iy = -1,-1

def setup():
    """
    Loads the datasets and DCNN model.
    In the case a model isn't saved in data, trains a new model.
    """
    global trainloader, net

    transform = transforms.Compose([transforms.ToTensor()])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False, num_workers=2)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if os.path.isfile('/data/models/dcnn.pt'):
        print("Trained model found. Loading from saved state.")
        net = Net(*args, **kwargs)
        net.load_state_dict('/data/models/dcnn.pt')
        net.eval()
    else:
        print("Beginning training of new model.")
        net = Net().to(device)

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

        train(net, trainloader, device, optimizer, criterion, epochs=2)

        # fd, path = tempfile.mkstemp()        
        # torch.save(net.state_dict(), path)
        # os.close(fd)
        # with open('/data/models/dcnn.txt', 'w+') as file:
        #     shutil.copy(path, file)
        #     os.remove(path)
        #     print("Model saved to /data/models/dcnn.pt")
def change_pixel(event, x, y, flags, userdata):
    """
    Gets the coordinates of the pixel in the image clicked on and inverts the color values.
    Updates the shown image and Tensor separately.
    """
    global ix, iy, imgs, pixels_changed, change_history

    if( event == cv2.EVENT_LBUTTONDOWN):
        pixels_changed += 1
        ix,iy = x, y
        if pixels_changed <= 10:
            print("Pixels changed: %d" % pixels_changed)
            print("(%.02f, %.02f): %s" % (ix, iy, str(img[ix, iy, :])))
            change_history[(ix,iy)] = img[iy,ix,:]
            img[iy, ix, :] = [0.066, 0.341, -0.254]
            imgs[:,0,iy,ix] = 0.066
            imgs[:,1,iy,ix] = 0.341
            imgs[:,2,iy,ix] = -0.254
            print(change_history)
        else:
            print("Out of pixels to change.")

def reset():
    """
    Loads a new image and label from the dataset and initializes a new window with them
    """
    global classes, pixels_changed, change_history, ix, iy, imgs, change_history
    pixels_changed = 0
    change_history = dict()
    ix,iy = -1,-1  

    try:
        imgs, labels = iter(trainloader).next()
    except Exception as e:
        print(str(e))
    img = (imgs[0,:,:,:]).numpy()
    img = np.moveaxis(img, 0, -1)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    imgs, labels = imgs.to(device), labels.to(device)
    outputs = net(imgs)
    _, predicted = torch.max(outputs, 1)
    label = classes[predicted[0]]

    if labels[0] == predicted[0]:
        return imgs, img, label
    else:
        return reset()

if __name__ == '__main__':
    setup()

    imgs, img, label = reset()

    cv2.namedWindow(label, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(label, 900,900)
    cv2.moveWindow(label, 100,100)
    cv2.setMouseCallback(label,change_pixel)

    while(1):
        cv2.imshow(label, img)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        if k == ord('e'):
            print("Evaluating image...")
            with torch.no_grad():
                outputs = net(imgs)
                _, predicted = torch.max(outputs, 1)
                print("Predicted class of modified image: %s" % classes[predicted[0]])
                easygui.msgbox("Predicted class of modified image: %s" % classes[predicted[0]], 
                               "Prediction")
                print()
        if k == ord('r'):
            cv2.destroyWindow(label)
            imgs, img, label = reset()
            cv2.namedWindow(label, cv2.WINDOW_NORMAL)
            cv2.resizeWindow(label, 900,900)
            cv2.moveWindow(label, 100,100)
            cv2.setMouseCallback(label,change_pixel)

    cv2.destroyAllWindows()