# **Level 4 Project & Dissertation**: A Seamful Game Based on PyTorch

---
## **Project**
---
This project was developed a University of Glasgow Level 4 Honors Project by Chase Condon, under the supervision of Prof. Matthew Chalmers.

The project was based on the idea of a "Seamful Game": a game that uses the shortcomings, or "seams", of a piece of software or hardware as a core feature. The game tasks players with tricking out a trained Deep Convolutional Neural Network by modifying a correctly classified image. Players are given a certain amount of time and actions to change pixel colors in a way that they think will cause the network to misclassify the image. The player is given real time feedback in the form of updated classification weights, shown both in numerical and continuous graph format.

The game serves as the tool from which research can be conducted into human understanding of image classifying neural networks, and deep learning in general. It trains them in a natural way to understand how the network came to its conclusion, and where the Seams exist.

---
### Gameplay
---

The player is presented with a screen containing a list of class labels and the current weight of classification, a graph showing the history of those label weights, and 32x32 pixel image that's being classified. The player then clicks on pixels to change their color values. The weights and graph automatically recalculate to show the current classification of the image by the Neural Network. 

The player can also undo a move at the cost of one action (unless on easy), and if on easy can completely reset the game to the beginning of the round. The player can also modify their pixel brush's size and color in the menus.


The difference in gameplay by difficulty is:

| Difficulty  | Time        |Actions   | Can reset image / actions | Undo move cost | Max brush size    | Colors avaliable           |
|-------------|:-----------:|:--------:|:-------------------------:|:--------------:|:-----------------:|:--------------------------:|
|**Easy**     |Unlimited    |20        | Yes                       | 0              | 3x3 pixel square  | All colors                 |
|**Medium**   |3 minutes    |20        | No                        | 1              | 3x3 pixel cross   | Primary + Secondary colors |
|**Hard**     |1 minute     |20        | No                        | 1              | 1 pixel           | Primary colors             |

---
### How to run
---
NetBreak can be played by going into the folder of the version you want to play, and running the **Main.py** script found there using Python 3 (python3 Main.py).

When running the game for the first time on v2 or above, the game will need to train a new DCNN model, which can take up to several minutes depending on your hardware. After this, the model is saved to file and you will be able to start the game without training a new model. 

The exception to this is Version 1 which did not implement a save feature for the model and will train a new model at the start of every run.

**Dependencies**
If you are missing any dependencies for NetBreak! you can resolve this by running **pip install -r dependencies** in the home folder of the project. This will install any missing dependencies.
---
### Analysis Tool
---

V3 of the Project and on have an additional Analysis Tool included that allows for analysis of previous plays of the game via the stored logs. The tool can be run using the **Main.py** script found in the versions **'Analysis Tool'** folder. Specifics of the tool are detailed in each version's **CHANGES.MD**.