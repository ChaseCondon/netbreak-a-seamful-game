# Changes in V4

## UI
- Added an undo button
    - disabled by default, enables when a action has been made and re-disables when clicked and an action is undone
- Timer now counts down on Medium and Hard difficulties
- Evaluate button was renamed to Give Up
- Evaluation popup has different text for giving up, timeout, and out of actions
- Reset Button Removed on Hard
- Give up button removed on Medium and Hard (possibly will revert)
- Pixels renamed to actions

## Functionality
- Difficulties have different settings:
    - Easy
        - No time limit
        - Can reset to start
        - undo doesn't cost action
    - Medium
        - 3 minute time limit
        - no reset
        - undo costs 1 action
    - Hard
        - 1 minute time limit
        - no reset
        - undo costs 1 action 
- Added undo functionality
    - costs an action
- Entire move history now stored and exported to log as a list called 'move_history'
    - comprised of dicts for each move with keys:
        - 'img': the image array at the current move
        - 'graph_data': the classes and their history of weights up to the current move
        - 'labels': the classes and their weights at the current move