import copy
import torch
import torch.nn.functional  as F
import numpy                as np
import matplotlib.pyplot    as plt

from kivy.app               import App
from Enums                  import Size, Difficulty
from kivy.uix.image         import Image
from math                   import floor
from kivy.graphics.texture  import Texture
from kivy.uix.boxlayout     import BoxLayout

from kivy.garden.graph      import Graph, LinePlot

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

class GameImage(Image):
    shown_img = None
    shown_arr = None
    real_arr = None
    tensor = None
    last_arr = None
    last_real = None
    last_tensor = None
    net = None
    label = None
    classification = None
    class_percents = None
    graph = None
    graph_max = 0.0
    undo = None
    move_history = []

    rgb = [255, 0, 0]
    brush_size = Size.SMALL
    remaining = None

    def retrieve_state(self):
        return self.tensor

    def get_labels(self):
        return self.label

    def get_arr(self):
        return self.real_arr

    def set_graph(self, graph):
        self.graph = graph

    def get_graph_data(self):
        return self.class_percents

    def get_move_history(self):
        return self.move_history

    def append_move(self):
        print('appending move')
        self.move_history.append({
            'img': copy.deepcopy(self.real_arr),
            'graph_data': copy.deepcopy(self.class_percents),
            'labels': copy.deepcopy(self.label)
        })

    def init_game(self, imgs, img, net, classification):
        self.classification = classification
        self.net = net
        self.move_history = []
        self.reset_image(imgs, img)
        App.get_running_app().time = 0
        App.get_running_app().ready=True

    def reset_image(self, imgs, img):
        global class_colors

        self.remaining = 20
        App.get_running_app().remaining = "Remaining actions\n" + str(self.remaining)
        self.changes = list()

        self.tensor = copy.deepcopy(imgs)

        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.real_arr = (img*255).astype("uint8")
        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img

        self.last_arr = None
        self.last_real = None
        self.last_tensor = None

        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            self.label = probs_list
            App.get_running_app().classification = "[u]Classification[/u]\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), probs_list))

        self.class_percents = dict(zip([a[0] for a in self.label], [[float(a[1])] for a in self.label]))
        
        for k, v in self.class_percents.items(): 
            self.graph_max = max(self.graph_max, v[0])
            self.class_percents[k].append(v[0])

        self.update_graph()
        self.append_move()
    
    def change_color(self, value):
        self.rgb = value

    def change_brush_size(self, size):
        self.brush_size = Size(size)

    def undo_change(self):
        if self.undo is not None:
            self.shown_arr = self.undo['last_arr']
            self.real_arr = self.undo['last_real']
            self.tensor = self.undo['last_tensor']
            self.undo = None

            if App.get_running_app().difficulty is not Difficulty.EASY:
                self.remaining -= 1
            else:
                self.remaining += 1
            App.get_running_app().root.ids.undo.disabled = True

            self.update_screen()
            self.append_move()

    def change_pixel(self, y, x):
        self.real_arr[y, x, :] = self.rgb

        self.tensor[:,0,y,x] = (self.rgb[0])/255
        self.tensor[:,1,y,x] = (self.rgb[1])/255
        self.tensor[:,2,y,x] = (self.rgb[2])/255

    def update_pixel(self, y, x):
        if self.real_arr[y,x,0] == self.rgb[0] and self.real_arr[y,x,1] == self.rgb[1] and self.real_arr[y,x,2] == self.rgb[2]:
            return
        self.remaining -= 1
        if self.remaining is 0:
            App.get_running_app().evaluate(out_of_actions=True)

        App.get_running_app().root.ids.undo.disabled = False
        self.undo = {'last_arr': copy.deepcopy(self.shown_arr),
                     'last_real': copy.deepcopy(self.real_arr),
                     'last_tensor': copy.deepcopy(self.tensor)}

        self.change_pixel(y, x)

        if self.brush_size is Size.MEDIUM or self.brush_size is Size.LARGE:
            if y is not 0:
                self.change_pixel(y-1, x)

            if y+1 is not 32:
                self.change_pixel(y+1, x)

            if x is not 0:
                self.change_pixel(y, x-1)

            if x+1 is not 32:
                self.change_pixel(y, x+1)

            if self.brush_size is Size.LARGE:
                if y is not 0 and x is not 0:
                    self.change_pixel(y-1, x-1)

                if y+1 is not 32 and x+1 is not 32:
                    self.change_pixel(y+1, x+1)

                if x is not 0 and y+1 is not 32:
                    self.change_pixel(y+1, x-1)

                if x+1 is not 32 and y is not 0:
                    self.change_pixel(y-1, x+1)

        self.update_screen()
        self.append_move()

    def update_screen(self):
        global class_colors

        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img
        
        App.get_running_app().remaining = "Remaining actions\n" + str(self.remaining)
        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            self.label = probs_list
            App.get_running_app().classification = "[u]Classification[/u]\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), probs_list))

        for i in range(10):
            self.class_percents[self.label[i][0]].append(float(self.label[i][1]))
            self.graph_max = max(self.graph_max, float(self.label[i][1]))

        self.update_graph()

        if self.label[0][0] != self.classification:
            App.get_running_app().evaluate()

    def hex_to_rgb(self, hex):
        r = int(hex[1:3], 16)/255
        g = int(hex[3:5], 16)/255
        b = int(hex[5:7], 16)/255
        return [r, g, b]

    def update_graph(self):
        global class_colors
        graph_space = App.get_running_app().root.ids.graph
        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=0, xmax=21-self.remaining, ymin=0, ymax=self.graph_max+.025)
        for _ in range(10):
            color = self.hex_to_rgb(class_colors[self.label[_][0]])
            plot = LinePlot(color=color, line_width=4)
            ys = self.class_percents[self.label[_][0]]
            plot.points = [ (i, ys[i]) for i in range(0, len(ys)) ]
            graph.add_plot(plot)
        graph_space.remove_widget(self.graph)
        self.graph = graph
        graph_space.add_widget(graph)
    
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            # Account for screen scaling
            # Image will always keep resolution of multiples of 32x32
            # Therefor the scale of the image is the minimum of the x and y scalars
            x_scale = self.width / 32
            y_scale = self.height / 32
            true_scale = min(x_scale, y_scale)
            if true_scale == x_scale:
                true_x = self.x
                true_y = self.y + (self.height-true_scale*32)/2
            else:
                true_y = self.y
                true_x = self.x + (self.width-true_scale*32)/2

            # The collide point for the widget isn't the collide point for the image
            # when the x and y scalars aren't equal. This takes into account the true
            # scale and image's true x and y
            if touch.x >= true_x and touch.x <= true_x + 32*true_scale and \
               touch.y >= true_y and touch.y <= true_y + 32*true_scale:
                y = floor((32*true_scale-(touch.y - true_y))/true_scale)
                x = floor((touch.x - true_x)/true_scale)
            
                self.update_pixel(y, x)
