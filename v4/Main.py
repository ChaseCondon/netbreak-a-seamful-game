# Whole library imports
import os
import uuid
import torch
import pickle
import torchvision
import torch.nn.functional      as F
import numpy                    as np
import torch.nn                 as nn
import torch.optim              as optim
import torchvision.transforms   as transforms

# Single function / class imports
from math                       import floor, sin, inf

# Custom imports
from ImageWidget                import GameImage
from NeuralNet                  import Net, train
from Enums                      import Difficulty, Size, Color
from CustomWidgets              import EasyBrushPopup, MediumBrushPopup, HardBrushPopup, NamePopup, CustomToggle, AlignedTextInput, ExitPopup

# Kivy app utility classes
from kivy.app                   import App
from kivy.clock                 import Clock
from kivy.config                import Config
from kivy.core.window           import Window
from kivy.properties            import StringProperty
from kivy.utils                 import get_color_from_hex
from kivy.garden.graph          import Graph, MeshLinePlot

# Kivy Interface classes
from kivy.uix.popup             import Popup
from kivy.uix.label             import Label
from kivy.uix.boxlayout         import BoxLayout
from kivy.uix.button            import Button

# This needs to be set prior to the app running. It's conventional to place this following the imports.
# This sets a non-resizable app window of 1162 x 1020 pixels. 
# This is needed to ensure pixel clicks on the image are correctly registered.
scaling_factor = 30
x_size = str( 32*scaling_factor*2 + 40 )
y_size = str( floor( (32*scaling_factor) * 1.25 )+25)

# Set global variables needed throughout the app program classes
trainloader = None
testloader = None
net = None
imgs = None
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

def setup():
    """
    Loads the datasets and DCNN model.
    In the case a model isn't saved in data, trains a new model.
    """
    global trainloader, testloader, net

    transform = transforms.Compose([transforms.ToTensor()])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=True, num_workers=2)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if os.path.isfile('./data/models/dcnn.pt'):
        print("Trained model found. Loading from saved state.")
        net = Net().to(device)
        net.load_state_dict(torch.load('./data/models/dcnn.pt'))
        net.eval()
    else:
        print("Beginning training of new model.")
        net = Net().to(device)

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

        train(net, trainloader, device, optimizer, criterion, epochs=2)
   
        torch.save(net.state_dict(), './data/models/dcnn.pt')
        print("Model saved to /data/models/dcnn.pt")


def reset():
    """
    Loads a new image and label from the dataset and initializes a new window with them
    """
    global classes, imgs, testloader, class_colors

    try:
        imgs, labels = iter(testloader).next()
    except:
        pass
        # print(str(e))
    img = (imgs[0,:,:,:]).numpy()
    img = np.moveaxis(img, 0, -1)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    imgs, labels = imgs.to(device), labels.to(device)
    outputs = net(imgs)
    _, predicted = torch.max(outputs, 1)

    if labels[0] == predicted[0]:
        probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
        probs_list = [a for a in probs_set]
        probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
        # print("\n".join(map(lambda a: '[color=%s]%s[/color]'%(class_colors[a[0]], str(a)), probs_list)))
        return imgs, img, probs_list
    else:
        return reset()


class GameApp(App):
    title = 'NetBreak!'
    player_name = False
    ready = False
    difficulty = None
    brush_popup = None
    imgs, img, label = None, None, None
    classification = StringProperty()
    orig_class = StringProperty()
    remaining = StringProperty()
    time = 0
    max_time = {Difficulty.EASY: inf,
                Difficulty.MEDIUM: 180,
                Difficulty.HARD: 60}
    time_label = StringProperty()
    score = [0, 0]
    score_label = StringProperty()
    evaluation_text = StringProperty()

    gameplay_log = list()

    def new_image(self, _):
        global net, class_colors

        self.imgs, self.img, self.labels = reset()
        self.classification = "Classification\n" + "\n".join(map(lambda a: "[color=%s]%s: %s[/color]"%(class_colors[a[0]], a[0], a[1]), self.labels))
        self.orig_class = "Original class\n" + self.labels[0][0]
        self.root.ids.img.init_game(self.imgs, self.img, net, self.labels[0][0])

    def evaluate(self, gave_up=None, out_of_actions=None):
        global classes
        
        self.ready = False
        modified_tensor = self.root.ids.img.retrieve_state()

        print(np.array_equal(imgs.cpu().numpy(), modified_tensor.cpu().numpy()))
        
        with torch.no_grad():
            outputs = net(modified_tensor)
            _, predicted = torch.max(outputs, 1)

        if classes[predicted[0]]!= self.labels[0][0]:
            self.score[0] += 1
            self.evaluation_text = "You fooled the Neural Net!\nThe Neural Net thought the image was a %s" % classes[predicted[0]]
        elif gave_up:
            self.evaluation_text = "You gave up!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        elif out_of_actions:
            self.evaluation_text = "You ran out of actions!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        else:
            self.evaluation_text = "Times up!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        self.score_label = "Score\n" + str(self.score[0]) + " / " + str(self.score[1])

        self.gameplay_log.append({ 'original_image': (self.img*255).astype('uint8'),
                                   'modified_image': self.root.ids.img.get_arr(),
                                   'original_outputs': self.labels,
                                   'modified_outputs': self.root.ids.img.get_labels(),
                                   'original_class': self.labels[0][0],
                                   'modified_class': self.root.ids.img.get_labels()[0][0],
                                   'move_history': self.root.ids.img.get_move_history(),
                                   'graph_data': self.root.ids.img.get_graph_data(),
                                   'time': self.time
                                 })

        popup = ExitPopup(title='Evaluation', size_hint=(None,None), size=(1000, 400), auto_dismiss=False)
        popup.bind(on_dismiss=self.new_image)
        popup.open()

    def undo(self):
        self.root.ids.img.undo_change()

    def reset(self):
        self.root.ids.img.reset_image(self.imgs, self.img)

    def change_brush_size(self, size):
        self.root.ids.img.change_brush_size(size)

    def change_brush_color(self, color):
        if isinstance(color, str):
            self.root.ids.img.change_color(Color[color.upper()].value)
        else:
            self.root.ids.img.change_color([floor(a*255) for a in color][0:3])

    def setup_brush(self):     

        if self.difficulty is Difficulty.EASY:
            self.brush_popup = EasyBrushPopup()

        if self.difficulty is Difficulty.MEDIUM:
            self.brush_popup = MediumBrushPopup()

        if self.difficulty is Difficulty.HARD:
            self.brush_popup = HardBrushPopup()

    def update_time(self, nap):

        if self.ready:
            self.time += nap
            if self.difficulty == Difficulty.EASY:
                minutes, seconds = divmod(self.time, 60)
            else:
                minutes, seconds = divmod(self.max_time[self.difficulty]-self.time, 60)
            self.root.ids.time.text = (
                'Time\n%02d:%02d.[size=20]%02d[/size]' %
                (int(minutes), int(seconds),
                int(seconds * 100 % 100))
            )
            if  int(minutes) == 0 and int(seconds) == 0 and int(seconds * 100 % 100) == 0:
                self.evaluate()

    def set_difficulty(self, difficulty):
        self.difficulty = Difficulty(difficulty)

    def start_game(self, name):
        Clock.schedule_interval(self.update_time, 0)
        self.player_name = name
        self.setup_brush()

        # Game settings by difficulty
        if self.difficulty is Difficulty.EASY:
            self.root.ids.img.rgb = [255, 255, 255]
        if self.difficulty is Difficulty.MEDIUM or self.difficulty is Difficulty.HARD:
            self.root.ids.buttons.remove_widget(self.root.ids.give_up)
        if self.difficulty is Difficulty.HARD:
            self.root.ids.buttons.remove_widget(self.root.ids.reset)

    def on_start(self):
        global net, class_colors

        self.imgs, self.img, self.labels = reset()
        self.classification = "Classification\n" + "\n".join(map(lambda a: "[color=%s]%s: %s[/color]"%(class_colors[a[0]], a[0], a[1]), self.labels))
        self.orig_class = "Original class\n" + self.labels[0][0]
        self.remaining = "Remaing actions\n20"
        self.score_label = "Score\n0 / 0"
        
        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=1, xmax=20, ymin=0, ymax=1)
        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, x*.05) for x in range(1, 20)]
        graph.add_plot(plot)
        self.root.ids.img.set_graph(graph)
        self.root.ids.graph.add_widget(graph)

        self.root.ids.img.init_game(self.imgs, self.img, net, self.labels[0][0])


    def stop(self, *largs):
        if isinstance(self.player_name, str) and len(self.gameplay_log) != 0:
            if self.player_name == '':
                self.player_name = uuid.uuid4()
            filename = './logs/' + self.player_name + '.pickle'

            print('Saving file: %s'%filename)

            i = 1
            while os.path.isfile(filename):
                filename = './logs/' + self.player_name + '_' + str(i) + '.pickle'
                i = i+1
                print('File exists, saving file: %s'%filename)

            with open(filename, 'wb+') as handle:
                pickle.dump(self.gameplay_log, handle, protocol=pickle.HIGHEST_PROTOCOL)

            print('File pickled')


if __name__ == '__main__':
    Window.pos = (200, 200)
    Window.size = (2400, 1200)
    Window.clearcolor = (0, 0, 0, 1)

    setup()

    GameApp().run()
