"""
The Neural Network and accompanying training method utilized by the NetBreak! game application

Slightly modified from the PyTorch classifier tutorial network which can be found at:
https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html
"""

import torch
import torchvision
import torchvision.transforms as transforms

import torch.nn               as nn
import torch.nn.functional    as F

import torch.optim            as optim

import matplotlib.pyplot      as plt
import numpy                  as np


class Net(nn.Module):
    """
    The Network class for the PyTorch tutorial classifier.
    """
    def __init__(self):
        """
        Initializes the various network layers
        """

        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 12, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(12, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
        
    def forward(self, x):
        """
        Feeds an input tensor through the network

        Parameters:
            x - the input image tensor. Expected to be a 3 channel 2d image

        Returns:
            x - a 1x10 tensor containing the output weights of the network.
        """
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def train(net, trainloader, device, optimizer, criterion, epochs=2):
    """
    Trains the network through backpropogation over the desired number of training epochs.

    Parameters:
        net         - the network being trained
        trainloader - the labeled image dataset 
        device      - the device to run the network on (cpu or cuda, based on gpu avaliability)
        optimizer   - the optimization class utilized for backpropogation
        criterion   - the loss function for evaluation of the network output
        epochs      - the number of training epochs

    Returns:
        true - if training has completed successfully
    """
    for epoch in range(epochs):
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            
            #zero the parameter gradients
            optimizer.zero_grad()
            
            #forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            
            #print(statistics)
            running_loss += loss.item()
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0
        
    return True