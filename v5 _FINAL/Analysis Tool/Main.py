"""
A tool used for reading and displaying the contents of NetBreak! log files
"""

import pickle
import numpy                as np

from kivy.app               import App
from math                   import floor
from kivy.uix.popup         import Popup
from kivy.uix.label         import Label
from kivy.uix.button        import Button
from kivy.uix.boxlayout     import BoxLayout
from kivy.properties        import StringProperty
from kivy.graphics.texture  import Texture   
from kivy.core.window       import Window
from kivy.garden.graph      import Graph, LinePlot
from kivy.clock             import Clock

class ErrorPopup(Popup):
    """
    A placeholder class for the popup displayed when an invalid file is attempted to be loaded.
    """

    pass


class AnalysisApp(App):
    """
    The main Kivy Application class for the Analysis tool
    """

    file = []
    current_index = 0
    orig_class = StringProperty()
    new_class = StringProperty()
    numbers = StringProperty()
    orig_label = StringProperty()
    mod_label = StringProperty()
    time = StringProperty()
    total_score = StringProperty()
    total_time = StringProperty()

    current_move = 0
    current_label = StringProperty()
    graph = None
    graph_max = 0

    def update_compare_screen(self):
        """
        Updates the widgets on the Comparison (high level) screen to reflect the images from the log file for the current round.
        """

        classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
        class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

        # Get the current image round from the loaded log file
        current_round = self.file[self.current_index]

        # Update starting and final classification weights
        self.orig_class = "Original\nClassification\n\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), current_round['original_outputs']))
        self.new_class = "Modified\nClassification\n\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), current_round['modified_outputs']))

        # If the player broke the classification, put classification labels in green
        # If not, put them in red
        if current_round['original_class'] == current_round['modified_class']:
            color = '#FF0000'
        else:
            color = '#00FF00'

        self.orig_label = '[color=%s]Original Class:\n%s[/color]'%(color, current_round['original_class'])
        self.mod_label = '[color=%s]Modified Class:\n%s[/color]'%(color, current_round['modified_class'])
        minutes, seconds = divmod(current_round['time'], 60)
        self.time = (
            'Time\n%02d:%02d.[size=20]%02d[/size]' %
            (int(minutes), int(seconds),
            int(seconds * 100 % 100))
        )

        # Render the starting and final image arrays
        shown_orig_img = Texture.create(size=(32, 32), colorfmt="rgb")
        shown_orig = np.flipud(current_round['original_image'])
        shown_orig_img.blit_buffer(shown_orig.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.root.ids.original.texture = shown_orig_img

        shown_mod_img = Texture.create(size=(32, 32), colorfmt="rgb")
        shown_mod = np.flipud(current_round['modified_image'])
        shown_mod_img.blit_buffer(shown_mod.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.root.ids.modified.texture = shown_mod_img

        # Set the graph max here even though it doesn't display as overall round data is modified here
        for move in current_round['move_history']:
            for _, v in move['graph_data'].items():
                self.graph_max = max(self.graph_max, max(v))

    def change_index(self, direction):
        """
        Performs bounds checking and updates the index of the currently displayed image, 
        updating all screens to the new image round.

        The move screen is always reset to 0 to avoid out of bounds errors with rounds of different lengths

        Parameters:
            direction - the direction to increment the round index, -1 for previous and 1 for next.   
        """

        if len(self.file) == 0:
            return
        if direction == -1 and self.current_index == 0:
            return
        if direction == 1 and self.current_index == len(self.file)-1:
            return
        self.current_index += direction
        self.current_move = 0
        self.update_compare_screen()
        self.update_move_screen()

    def hex_to_rgb(self, hex):
        """
        Converts a hex string to a list of RGB values

        Parameters:
            hex - the hexidecimal color string

        Returns:
            [r, g, b] - array of integer RGB color values
        """

        r = int(hex[1:3], 16)/255
        g = int(hex[3:5], 16)/255
        b = int(hex[5:7], 16)/255
        return [r, g, b]

    def update_move_screen(self):
        """
        Updates the move (low level) screen to match the game state at the current move index.
        """

        classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
        class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

        # Gets move data for the current move in the current round
        cur_move = self.file[self.current_index]['move_history'][self.current_move]

        # Updates label weights
        self.current_label = "Classification\n\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), cur_move['labels']))

        # Renders the image array for the current move
        shown_orig_img = Texture.create(size=(32, 32), colorfmt="rgb")
        shown_orig = np.flipud(cur_move['img'])
        shown_orig_img.blit_buffer(shown_orig.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.root.ids.current_img.texture = shown_orig_img

        # Creates a graph widget and plots the graph data up to the current move
        graph_space = self.root.ids.graph
        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=0, xmax=self.current_move+1, ymin=0, ymax=self.graph_max+.025)
        for _ in range(10):
            color = self.hex_to_rgb(class_colors[cur_move['labels'][_][0]])
            plot = LinePlot(color=color, line_width=4)
            ys = cur_move['graph_data'][cur_move['labels'][_][0]][0:self.current_move+2]
            plot.points = [ (i, ys[i]) for i in range(0, len(ys)) ]
            graph.add_plot(plot)
        graph_space.remove_widget(self.graph)
        self.graph = graph
        graph_space.add_widget(graph)

    def change_move(self, direction):
        """
        Performs bounds checking and updates the index of the currently displayed move.

        Parameters:
            direction - the direction to increment the round index, -1 for previous and 1 for next.   
        """

        if len(self.file[self.current_index]) == 0:
            return
        if direction == -1 and self.current_move == 0:
            return
        if direction == 1 and self.current_move == len(self.file[self.current_index]['move_history'])-1:
            return
        self.current_move += direction
        self.update_compare_screen()
        self.update_move_screen()

    def open_file(self, path, selection):
        """
        Opens the selected file and initializes some overall statistics such as total game time and score

        Parameters:
            path      - the path to the selected file
            selection - the name of the file selected
        """ 

        filepath = selection[0]
        self.root.current = 'view'

        print(filepath)
        print(filepath.split('.'))

        # Check for correct filetypes before reading, giving an error popup if an invalid file is selected
        if len(filepath.split('.')) == 1 or 'pickle' not in filepath.split('.'):
            popup = ErrorPopup(title='Evaluation',
                      size_hint=(None, None), 
                      size=(1000, 400))
            popup.open()
            return
        with open(filepath, 'rb') as picklefile:
            self.file = pickle.load(picklefile)
        print("Game of length %d rounds loaded."%len(self.file))

        total_score = [0, 0]
        total_time = 0

        # Sum the image round times and scores for the cumulative totals
        for i in range(len(self.file)):
            total_time += self.file[i]['time']
            if self.file[i]['original_class'] == self.file[i]['modified_class']:
                total_score[1] += 1
            else:
                total_score[0] += 1

        self.total_score = "Total Score\n%d / %d"%(total_score[0], total_score[1])

        minutes, seconds = divmod(total_time, 60)
        self.total_time = (
            'Total Time\n%02d:%02d.[size=20]%02d[/size]' %
            (int(minutes), int(seconds),
            int(seconds * 100 % 100))
        )

        # Update the screens for the new file
        self.update_compare_screen()
        self.update_move_screen()

    def change(self, direction, tab):
        """
        Choses what index to change based on the currently open screen (tab)
        """

        if tab == 'Compare Start / End':
            self.change_index(direction)
        if tab == 'Move History':
            self.change_move(direction)

    def _on_keyboard_down(self, instance, keyboard, keycode, text, modifiers):
        """
        Handles keyboard actions - changing the image / move index (based on current screen)
        """

        if keycode == 79:
            if self.root.ids.tabs.current_tab.text == 'Compare Start / End':
                self.change(1, self.root.ids.tabs.current_tab.text)
            else:
                self.change(1, self.root.ids.tabs.current_tab.text)
        if keycode == 80:
            if self.root.ids.tabs.current_tab.text == 'Move History':
                self.change(-1, self.root.ids.tabs.current_tab.text)
            else:
                self.change(-1, self.root.ids.tabs.current_tab.text)

    def update_all(self, nap):
        """
        Called on a regular timer. Ensures that the index counters are always displaying correctly
        """

        if self.file:
            if self.root.ids.tabs.current_tab.text == 'Compare Start / End':
                self.numbers = 'Image: ' + str(self.current_index+1) + ' / ' + str(len(self.file))
            else:
                self.numbers = 'Move: ' + str(self.current_move) + ' / ' + str(len(self.file[self.current_index]['move_history'])-1)
        else:
            self.numbers = '0 / 0'      

    def on_start(self):
        """
        Called when the application starts. This sets default labels and widgets, and begins the internal clock
        """

        self.orig_class = 'Original\nClassification'
        self.new_class = "Modified\nClassification"
        self.numbers = '0 / 0'
        self.orig_label = 'Original Class:\n-'
        self.mod_label = "Modified Class:\n-"
        self.time = 'Time\n00:00.[size=20]00[/size]'
        self.total_time = 'Total Time\n00:00.[size=20]00[/size]'
        self.total_score = 'Score\n0 / 0'
        self.current_label = 'Class:\n-'

        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=1, xmax=20, ymin=0, ymax=1)
        plot = LinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, x*.05) for x in range(1, 20)]
        graph.add_plot(plot)
        self.graph = graph
        self.root.ids.graph.add_widget(graph)

        Clock.schedule_interval(self.update_all, 0)


        Window.bind(on_key_down=self._on_keyboard_down)
        

if __name__ == '__main__':
    Window.pos = (200, 200)
    Window.size = (2400, 1200)

    AnalysisApp().run()