"""
This is the primary file for the NetBreak! game platform.

This file can be run via the command:
    python3 Main.py
"""

# Whole library imports
import os
import uuid
import torch
import pickle
import torchvision
import torch.nn.functional      as F
import numpy                    as np
import torch.nn                 as nn
import torch.optim              as optim
import torchvision.transforms   as transforms

# Single function / class imports
from math                       import floor, sin, inf

# Customs imports
from ImageWidget                import GameImage
from NeuralNet                  import Net, train
from Enums                      import Difficulty, Size, Color
from CustomWidgets              import EasyBrushPopup, MediumBrushPopup, HardBrushPopup, NamePopup, CustomToggle, AlignedTextInput, ExitPopup

# Kivy app utility classes
from kivy.app                   import App
from kivy.clock                 import Clock
from kivy.config                import Config
from kivy.core.window           import Window
from kivy.properties            import StringProperty
from kivy.utils                 import get_color_from_hex
from kivy.garden.graph          import Graph, MeshLinePlot

# Kivy Interface classes
from kivy.uix.popup             import Popup
from kivy.uix.label             import Label
from kivy.uix.boxlayout         import BoxLayout
from kivy.uix.button            import Button

# This sets the default window size based on an initial scaling factor
# On a standard 1920x1080 HD monitor, this will fill the whole screen
scaling_factor = 30
x_size = str( 32*scaling_factor*2 + 40 )
y_size = str( floor( (32*scaling_factor) * 1.25 )+25)

# Set global variables needed throughout the app program classes
trainloader = None
testloader = None
net = None
imgs = None
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

def setup():
    """
    Loads the datasets and DCNN model.
    In the case a model isn't saved in data, trains a new model.
    """
    global trainloader, testloader, net

    transform = transforms.Compose([transforms.ToTensor()])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=True, num_workers=2)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if os.path.isfile('./data/models/dcnn.pt'):
        print("Trained model found. Loading from saved state.")
        net = Net().to(device)
        net.load_state_dict(torch.load('./data/models/dcnn.pt'))
        net.eval()
    else:
        print("Beginning training of new model.")
        net = Net().to(device)

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

        train(net, trainloader, device, optimizer, criterion, epochs=2)
   
        torch.save(net.state_dict(), './data/models/dcnn.pt')
        print("Model saved to /data/models/dcnn.pt")


def reset():
    """
    Retrieves a new image and its classification weights as a sorted list of string formated decimals.
    Recursively is called until an image that was correctly classified by the network is selected.

    Returns:
        imgs       - the image vector
        img        - a numpy representation of the image vector readable by Kivy's Image 
        probs_list - a sorted list of the network's output weights, string formatted to 4 decimal places
    """
    global classes, imgs, testloader, class_colors

    try:
        imgs, labels = iter(testloader).next()
    except:
        pass
        # print(str(e))
    img = (imgs[0,:,:,:]).numpy()
    img = np.moveaxis(img, 0, -1)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    imgs, labels = imgs.to(device), labels.to(device)
    outputs = net(imgs)
    _, predicted = torch.max(outputs, 1)

    if labels[0] == predicted[0]:
        probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
        probs_list = [a for a in probs_set]
        probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
        # print("\n".join(map(lambda a: '[color=%s]%s[/color]'%(class_colors[a[0]], str(a)), probs_list)))
        return imgs, img, probs_list
    else:
        return reset()


class GameApp(App):
    """
    The main Kivy Application class. This handles all primary game functionality and acts 
    as a container for all widgets.
    """
    title = 'NetBreak!'
    player_name = ''
    ready = False
    difficulty = None
    brush_popup = None
    imgs, img, label = None, None, None
    classification = StringProperty()
    orig_class = StringProperty()
    remaining = StringProperty()
    time = 0
    max_time = {Difficulty.EASY: inf,
                Difficulty.MEDIUM: 180,
                Difficulty.HARD: 60}
    time_label = StringProperty()
    score = [0, 0]
    score_label = StringProperty()
    evaluation_text = StringProperty()

    gameplay_log = list()

    def new_image(self, _):
        """
        Retrieves a new image from the reset function and sets up the application labels and image screen with the new image data.
        """
        global net, class_colors

        self.imgs, self.img, self.labels = reset()
        self.classification = "Classification\n" + "\n".join(map(lambda a: "[color=%s]%s: %s[/color]"%(class_colors[a[0]], a[0], a[1]), self.labels))
        self.orig_class = "Original class\n" + self.labels[0][0]
        self.root.ids.img.init_game(self.imgs, self.img, net, self.labels[0][0])

    def evaluate(self, gave_up=None, out_of_actions=None):
        """
        Runs the network evaluation.

        This retrieves a final classification of the image vector at its currently modified state and 
        compares it against the original output label, updating scores accordingly.

        In the case of the player hitting the give up button, running out of actions, or running out of time
        (specified by the input parameters), gives the respective popup window.

        Finally, all information for the log file of this move is gathered and appended to the overall game log.

        Parameters:
            gave_up        - Flagged if the player has hit the give up button on the user interface. 
                             Will trigger the loss score to update and the respective popup to show.
            out_of_actions - Flagged if the player has expended all of their actions. 
                             Will trigger the loss score to update and the respective popup to show.
        """
        global classes
        
        self.ready = False
        modified_tensor = self.root.ids.img.retrieve_state()

        print(np.array_equal(imgs.cpu().numpy(), modified_tensor.cpu().numpy()))
        
        with torch.no_grad():
            outputs = net(modified_tensor)
            _, predicted = torch.max(outputs, 1)

        if classes[predicted[0]]!= self.labels[0][0]:
            self.score[0] += 1
            self.evaluation_text = "You fooled the Neural Net!\nThe Neural Net thought the image was a %s" % classes[predicted[0]]
        elif gave_up:
            self.evaluation_text = "You gave up!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        elif out_of_actions:
            self.evaluation_text = "You ran out of actions!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        else:
            self.evaluation_text = "Times up!\nThe Neural Net still thinks the image is a %s" % classes[predicted[0]]
            self.score[1] += 1
        self.score_label = "Score\n" + str(self.score[0]) + " / " + str(self.score[1])

        # Create the game log
        self.gameplay_log.append({ 'original_image': (self.img*255).astype('uint8'),            # The numpy array of the original game image
                                   'modified_image': self.root.ids.img.get_arr(),               # The numpy array of the final game image
                                   'original_outputs': self.labels,                             # The original output weights
                                   'modified_outputs': self.root.ids.img.get_labels(),          # The final output weights
                                   'original_class': self.labels[0][0],                         # The original class label
                                   'modified_class': self.root.ids.img.get_labels()[0][0],      # The final class label
                                   'move_history': self.root.ids.img.get_move_history(),        # A list of all numpy arrays at each action
                                   'graph_data': self.root.ids.img.get_graph_data(),            # A list of the graph data (output weights) at each action
                                   'time': self.time                                            # The time expended for this image
                                 })

        popup = ExitPopup(title='Evaluation', size_hint=(None,None), size=(1000, 400), auto_dismiss=False)
        popup.bind(on_dismiss=self.new_image)   # Closing the popup will trigger the game to retrieve a new image and set the user interface back up
        popup.open()

    def undo(self):
        """
        Undoes the previous move
        """
        self.root.ids.img.undo_change()

    def reset(self):
        """
        Resets the image vector to its original state
        """
        self.root.ids.img.reset_image(self.imgs, self.img)

    def change_brush_size(self, size):
        """
        Changes the brush size

        Parameters:
            size - the size of brush to change to
        """
        self.root.ids.img.change_brush_size(size)

    def change_brush_color(self, color):
        """
        Changes the brush color

        Parameters:
            color - the color of brush to change to as either a String or a list of RGB value integers
        """
        if isinstance(color, str):
            self.root.ids.img.change_color(Color[color.upper()].value)
        else:
            self.root.ids.img.change_color([floor(a*255) for a in color][0:3])

    def setup_brush(self):
        """
        Sets the brush popup to trigger based on the selected difficulty
        """     

        if self.difficulty is Difficulty.EASY:
            self.brush_popup = EasyBrushPopup()

        if self.difficulty is Difficulty.MEDIUM:
            self.brush_popup = MediumBrushPopup()

        if self.difficulty is Difficulty.HARD:
            self.brush_popup = HardBrushPopup()

    def update_time(self, nap):
        """
        Updates the internal game clock and timer label based on the nap since last clock tick.

        In the case of medium or hard dificulties, subtracts the internal clock from the maximum time to 
        give a countdown timer.

        If the timer hits 0, trigger auto-evaluation

        Parameters:
            nap - the time in miliseconds to add to the timer.
        """
        if self.ready:
            self.time += nap
            if self.difficulty == Difficulty.EASY:
                minutes, seconds = divmod(self.time, 60)
            else:
                minutes, seconds = divmod(self.max_time[self.difficulty]-self.time, 60)
            self.root.ids.time.text = (
                'Time\n%02d:%02d.[size=20]%02d[/size]' %
                (int(minutes), int(seconds),
                int(seconds * 100 % 100))
            )
            if  int(minutes) == 0 and int(seconds) == 0 and int(seconds * 100 % 100) == 0:
                self.evaluate()

    def set_difficulty(self, difficulty):
        """
        Sets the game difficulty
        """
        self.difficulty = Difficulty(difficulty)

    def start_game(self):
        """
        Starts the game clock and sets up initial brush and UI settings based on difficulty
        """
        Clock.schedule_interval(self.update_time, 0)
        self.setup_brush()

        # Game settings by difficulty
        if self.difficulty is Difficulty.EASY:
            self.root.ids.img.rgb = [255, 255, 255]
        if self.difficulty is Difficulty.MEDIUM or self.difficulty is Difficulty.HARD:
        #     self.root.ids.buttons.remove_widget(self.root.ids.give_up)
            self.root.ids.buttons.remove_widget(self.root.ids.reset)

    def on_start(self):
        """
        Called when the application is initiated. 

        Sets the initial image vector and initializes UI Labels and Widgets.
        """

        global net, class_colors

        self.imgs, self.img, self.labels = reset()
        self.classification = "Classification\n" + "\n".join(map(lambda a: "[color=%s]%s: %s[/color]"%(class_colors[a[0]], a[0], a[1]), self.labels))
        self.orig_class = "Original class\n" + self.labels[0][0]
        self.remaining = "Remaining changes\n20"
        self.score_label = "Score\n0 / 0"
        
        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=1, xmax=20, ymin=0, ymax=1)
        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, x*.05) for x in range(1, 20)]
        graph.add_plot(plot)
        self.root.ids.img.set_graph(graph)
        self.root.ids.graph.add_widget(graph)

        self.root.ids.img.init_game(self.imgs, self.img, net, self.labels[0][0])


    def stop(self, *largs):
        """
        Called when the exit button has been clicked but before program execution has ended.

        Generates a UID for the player and pickles the game_history log under that UID.pickle in the logs folder.
        """
        print("Stopping game.")
        if isinstance(self.player_name, str) and len(self.gameplay_log) != 0:
            if self.player_name == '':
                self.player_name = str(uuid.uuid4()).split('-')[0]
            filename = './logs/' + self.player_name + '.pickle'

            print('Saving file: %s'%filename)

            i = 1
            while os.path.isfile(filename):
                filename = './logs/' + self.player_name + '_' + str(i) + '.pickle'
                i = i+1
                print('File exists, saving file: %s'%filename)

            with open(filename, 'wb+') as handle:
                pickle.dump(self.gameplay_log, handle, protocol=pickle.HIGHEST_PROTOCOL)

            print('File pickled')


if __name__ == '__main__':
    Window.pos = (200, 200)
    Window.size = (2400, 1200)
    Window.clearcolor = (0, 0, 0, 1)

    setup() # Set up the network and datasets prior to initializing the game

    GameApp().run() # Start the game app
