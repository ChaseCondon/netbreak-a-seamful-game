"""
Any custom Kivy widgets created for use in NetBreak!

Primarially made up of placeholder classes whose form and function are described in game.kv file.
"""

from kivy.uix.popup     import Popup
from kivy.uix.button    import Button
from kivy.uix.behaviors import ToggleButtonBehavior

from kivy.uix.textinput import TextInput
from kivy.properties import StringProperty


# Pass here, defined in game.kv
class EasyBrushPopup(Popup):
    """
    The brush popup menu displayed when a player is on the Easy difficulty
    """

    pass

# Pass here, defined in game.kv
class MediumBrushPopup(Popup):
    """
    The brush popup menu displayed when a player is on the Medium difficulty
    """

    pass


# Pass here, defined in game.kv
class HardBrushPopup(Popup):
    """
    The brush popup menu displayed when a player is on the Hard difficulty
    """

    pass


#Pass here, defined in game.kv
class NamePopup(Popup):
    """
    The popup displayed at before the game begins to collect a name for the log file

    Not utilized in NetBreak! v5
    """

    pass


#Pass here, defined in game.kv
class ExitPopup(Popup):
    """
    Displays when evaluation is triggered
    """

    pass


class CustomToggle(ToggleButtonBehavior, Button):
    """
    A custom Toggle button that only allows for one in a group to be selected at a time 
    and requires at least one in a group to be active

    Utilized for color and size buttons in the brush menus.
    """

    def __init__(self, **kwargs):
        super(CustomToggle, self).__init__(**kwargs)
        self.allow_no_selection = False

# ALIGNEDTEXTINPUT
# NOT MINE
# CREATED BY MELLEB
# https://gist.github.com/MelleB/4a46afe263969a49ce5dad078da55a87
# Allows for text in a text input box to be centrally aligned

DEFAULT_PADDING = 6

class AlignedTextInput(TextInput):

    halign = StringProperty('left')
    valign = StringProperty('top')

    def __init__(self, **kwargs):
        self.halign = kwargs.get("halign", "center")
        self.valign = kwargs.get("valign", "middle")

        self.bind(on_text=self.on_text)

        super().__init__(**kwargs)
        
    def on_text(self, instance, value):
        self.redraw()
        
    def on_size(self, instance, value):
        self.redraw()

    def redraw(self):
        """ 
        Note: This methods depends on internal variables of its TextInput
        base class (_lines_rects and _refresh_text())
        """

        self._refresh_text(self.text)
        
        max_size = max(self._lines_rects, key=lambda r: r.size[0]).size
        num_lines = len(self._lines_rects)
        
        px = [DEFAULT_PADDING, DEFAULT_PADDING]
        py = [DEFAULT_PADDING, DEFAULT_PADDING]
        
        if self.halign == 'center':
            d = (self.width - max_size[0]) / 2.0 - DEFAULT_PADDING
            px = [d, d]
        elif self.halign == 'right':
            px[0] = self.width - max_size[0] - DEFAULT_PADDING
            
        if self.valign == 'middle':
            d = (self.height - max_size[1] * num_lines) / 2.0 - DEFAULT_PADDING
            py = [d, d]
        elif self.valign == 'bottom':
            py[0] = self.height - max_size[1] * num_lines - DEFAULT_PADDING

        self.padding_x = px
        self.padding_y = py