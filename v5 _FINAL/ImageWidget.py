"""
A custom subclass of the Kivy's Image widget.

Implements visualization of a PyTorch tensor through the Image's texture map,
and handles player input and image modification.

Also oversees control of all internal variables surrounding image data such as graph data.
"""

import copy
import torch
import torch.nn.functional  as F
import numpy                as np
import matplotlib.pyplot    as plt

from kivy.app               import App
from Enums                  import Size, Difficulty
from kivy.uix.image         import Image
from math                   import floor
from kivy.graphics.texture  import Texture
from kivy.uix.boxlayout     import BoxLayout

from kivy.garden.graph      import Graph, LinePlot

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
class_colors = dict(zip(classes, ('#FD3A4A', '#FFAA1D', '#FFF700', '#A7F432', '#5DADEC', '#9C51B6', '#BFAFB2', '#E936A7', '#CCFF00', '#C32148')))

class GameImage(Image):
    """
    The image subclass
    """
    shown_img = None        # The texture map of the image currently being shown
    shown_arr = None        # The numpy array of the currently shown image
    real_arr = None         # The 
    tensor = None
    last_arr = None
    last_real = None
    last_tensor = None
    net = None
    label = None
    classification = None
    class_percents = None
    graph = None
    graph_max = 0.0
    undo = None
    move_history = []

    rgb = [255, 0, 0]
    brush_size = Size.SMALL
    remaining = None

    def retrieve_state(self):
        """
        Retrieves the current image tensor

        Returns:
            self.tensor - the internal image tensor
        """

        return self.tensor

    def get_labels(self):
        """
        Retrieves the current sorted list of formatted label weights

        Returns:
            self.label - the internal set of sorted current labels and formatted weights
        """

        return self.label

    def get_arr(self):
        """
        Retrieves the current numpy array of the image

        Returns:
            self.real_arr - the numpy array representing the internal tensor
        """

        return self.real_arr

    def set_graph(self, graph):
        """
        Sets the internal reference to the graph widget

        Parameters:
            graph - the graph widget object
        """

        self.graph = graph

    def get_graph_data(self):
        """
        Retrieves the history of label weights used for graphing

        Returns:
            self.class_percents - the record of all weights for each label over the time-step
        """

        return self.class_percents

    def get_move_history(self):
        """
        Retrieves the list of move history dictionaries

        Returns:
            self.move_history - a list of dictionaries containing the image state, graph data, and label data at each time-step
        """

        return self.move_history

    def append_move(self):
        """
        Adds the current image array, graph_data, and label weights as a dictionary to the move history
        """

        print('appending move')
        self.move_history.append({
            'img': copy.deepcopy(self.real_arr),
            'graph_data': copy.deepcopy(self.class_percents),
            'labels': copy.deepcopy(self.label)
        })

    def init_game(self, imgs, img, net, classification):
        """
        Initializes the GameImage widget with the image and classification.

        As this is called between rounds, also resets the round time to 0.

        Parameters:
            imgs           - the original image tensor 
            img            - the original image array
            net            - the neural network model
            classification - the original image label 
        """

        self.classification = classification
        self.net = net
        self.move_history = []
        self.reset_image(imgs, img)
        App.get_running_app().time = 0
        App.get_running_app().ready=True

    def reset_image(self, imgs, img):
        """
        Takes a given image tensor and array, and resets the Image widget to the base state using them.

        Parameters:
            imgs - the image tensor to be set
            img  - the image array to be set
        """
        
        global class_colors

        self.remaining = 20
        App.get_running_app().remaining = "Remaining changes\n" + str(self.remaining)
        self.changes = list()

        self.tensor = copy.deepcopy(imgs)

        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.real_arr = (img*255).astype("uint8")
        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img

        self.last_arr = None
        self.last_real = None
        self.last_tensor = None

        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            self.label = probs_list
            App.get_running_app().classification = "[u]Classification[/u]\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), probs_list))

        self.class_percents = dict(zip([a[0] for a in self.label], [[float(a[1])] for a in self.label]))
        
        for k, v in self.class_percents.items(): 
            self.graph_max = max(self.graph_max, v[0])
            self.class_percents[k].append(v[0])

        self.update_graph()
        self.append_move()
    
    def change_color(self, value):
        """
        Changes the color of the brush attack to the chosen value

        Parameters:
            value - the desired color as a list of integer rgb values [R, G, B]
        """

        self.rgb = value

    def change_brush_size(self, size):
        """
        Changes the size of the brush to the chosen size

        Parameters:
            size - the desired brush size as a string
        """

        self.brush_size = Size(size)

    def undo_change(self):
        """
        If there is a previous move recorded, resets the interal image state to the state of the previous move and sets the previous state to None.
        If the game difficulty is either Medium or Hard, the score is decremented as an undo action costs one action. If easy, an action point is returned.
        Finally, updates the screen to reflect internal changes and disables the undo button as there is no previous state to undo to.
        """

        if self.undo is not None:
            self.shown_arr = self.undo['last_arr']
            self.real_arr = self.undo['last_real']
            self.tensor = self.undo['last_tensor']
            self.undo = None

            if App.get_running_app().difficulty is not Difficulty.EASY:
                self.remaining -= 1
            else:
                self.remaining += 1
            App.get_running_app().root.ids.undo.disabled = True

            self.update_screen()
            self.append_move()

    def change_pixel(self, y, x):
        """
        Changes the values of the internal array and tensor at the specified pixel coordinate to the internal RGB color value

        Parameters:
            y - the y coordinate of the pixel to be changed
            x - the x coordinate of the pixel to be changed
        """

        self.real_arr[y, x, :] = self.rgb

        self.tensor[:,0,y,x] = (self.rgb[0])/255
        self.tensor[:,1,y,x] = (self.rgb[1])/255
        self.tensor[:,2,y,x] = (self.rgb[2])/255

    def update_pixel(self, y, x):
        """
        Selects what pixels to update based on specified coordinates, brush size, and current color, changes the selected pixels, and updates the screen.
    
        Parameters:
            y - the y coordinate of the pixel to be changed
            x - the x coordinate of the pixel to be changed
        """

        # If the pixel is already the internal color, it has already been changed and shouldn't be counted as an action
        if self.real_arr[y,x,0] == self.rgb[0] and self.real_arr[y,x,1] == self.rgb[1] and self.real_arr[y,x,2] == self.rgb[2]:
            return
        self.remaining -= 1         # Decrement the remaining actions
        if self.remaining is 0:     # Trigger auto-evaluation if the player is out of actions
            App.get_running_app().evaluate(out_of_actions=True)

        # Append the game state to the undo record before updating pixel values
        App.get_running_app().root.ids.undo.disabled = False
        self.undo = {'last_arr': copy.deepcopy(self.shown_arr),
                     'last_real': copy.deepcopy(self.real_arr),
                     'last_tensor': copy.deepcopy(self.tensor)}

        self.change_pixel(y, x)

        if self.brush_size is Size.MEDIUM or self.brush_size is Size.LARGE:
            if y is not 0:
                self.change_pixel(y-1, x)

            if y+1 is not 32:
                self.change_pixel(y+1, x)

            if x is not 0:
                self.change_pixel(y, x-1)

            if x+1 is not 32:
                self.change_pixel(y, x+1)

            if self.brush_size is Size.LARGE:
                if y is not 0 and x is not 0:
                    self.change_pixel(y-1, x-1)

                if y+1 is not 32 and x+1 is not 32:
                    self.change_pixel(y+1, x+1)

                if x is not 0 and y+1 is not 32:
                    self.change_pixel(y+1, x-1)

                if x+1 is not 32 and y is not 0:
                    self.change_pixel(y-1, x+1)

        self.update_screen()
        self.append_move()

    def update_screen(self):
        """
        Updates the various widget states including:
            - Updating the shown image widget's texture map to match the internal image array
            - Updating the classification labels based on a reclassification of the internal image tensor
            - Updating the graph widget with the new label weights
        """

        global class_colors

        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img
        
        App.get_running_app().remaining = "Remaining changes\n" + str(self.remaining)
        
        # Image tensor has been modified so reclassify.
        with torch.no_grad():
            outputs = self.net(self.tensor)
            probs_set = set( zip( classes, map(lambda x: "{0:.4f}".format(x), 
                                            F.softmax(outputs, dim=1).tolist()[0]) 
                             ) )
            probs_list = [a for a in probs_set]
            probs_list = sorted(probs_list, key=lambda x: x[1], reverse=True)
            self.label = probs_list
            App.get_running_app().classification = "[u]Classification[/u]\n" + "\n".join(map(lambda a: "[color=%s][b]%s[/b]: %s[/color]"%(class_colors[a[0]], a[0], a[1]), probs_list))

        # Update graph data
        for i in range(10):
            self.class_percents[self.label[i][0]].append(float(self.label[i][1]))
            self.graph_max = max(self.graph_max, float(self.label[i][1]))

        self.update_graph()

        # If the updated primary label does not match the stored original label, trigger auto-evaluation as the player has won.
        if self.label[0][0] != self.classification:
            App.get_running_app().evaluate()

    def hex_to_rgb(self, hex):
        """
        Converts a hex string to a list of RGB values

        Parameters:
            hex - the hexidecimal color string

        Returns:
            [r, g, b] - array of integer RGB color values
        """

        r = int(hex[1:3], 16)/255
        g = int(hex[3:5], 16)/255
        b = int(hex[5:7], 16)/255
        return [r, g, b]

    def update_graph(self):
        """
        Update the shown graph plot to match the internal graph data.

        This creates a new Graph widget to replace the existing one
        """

        global class_colors
        graph_space = App.get_running_app().root.ids.graph
        graph = Graph(x_grid_label=False, y_grid_label=False, x_grid=False,
                      y_grid=False, xmin=0, xmax=21-self.remaining, ymin=0, ymax=self.graph_max+.025)
        for _ in range(10):
            color = self.hex_to_rgb(class_colors[self.label[_][0]])
            plot = LinePlot(color=color, line_width=4)
            ys = self.class_percents[self.label[_][0]]
            plot.points = [ (i, ys[i]) for i in range(0, len(ys)) ]
            graph.add_plot(plot)
        graph_space.remove_widget(self.graph)
        self.graph = graph
        graph_space.add_widget(graph)
    
    def on_touch_down(self, touch):
        """
        Triggered on any touch action in the application. Checks for collision with the image widget and 
        uses image scales to find the coordinates of the clicked on pixel.

        Parameters:
            touch - package containing touch action data (ie. touch.pos - the screen coordinates of the touch action)
        """

        if self.collide_point(*touch.pos):
            # Account for screen scaling
            # Image will always keep resolution of multiples of 32x32
            # Therefor the scale of the image is the minimum of the x and y scalars
            x_scale = self.width / 32
            y_scale = self.height / 32
            true_scale = min(x_scale, y_scale)
            if true_scale == x_scale:
                true_x = self.x
                true_y = self.y + (self.height-true_scale*32)/2
            else:
                true_y = self.y
                true_x = self.x + (self.width-true_scale*32)/2

            # The collide point for the widget isn't the collide point for the image
            # when the x and y scalars aren't equal. This takes into account the true
            # scale and image's true x and y
            if touch.x >= true_x and touch.x <= true_x + 32*true_scale and \
               touch.y >= true_y and touch.y <= true_y + 32*true_scale:
                y = floor((32*true_scale-(touch.y - true_y))/true_scale)
                x = floor((touch.x - true_x)/true_scale)
            
                self.update_pixel(y, x)
