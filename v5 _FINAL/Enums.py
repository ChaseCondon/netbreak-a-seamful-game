"""
All enumerated types used in the game
"""

from enum import Enum

class Difficulty(Enum):
    """
    Enums for specifying game difficulty
    """

    EASY   = "Easy"
    MEDIUM = "Medium"
    HARD   = "Hard"


class Size(Enum):
    """
    Enums for specifying the brush size
    """

    SMALL   = "Small"
    MEDIUM  = "Medium"
    LARGE   = "Large"


class Color(Enum):
    """
    Enums for the valid brush color button options and their respective RGB values
    """

    RED     = [255, 0, 0]
    YELLOW  = [255, 255, 0]
    BLUE    = [0, 0, 255]
    ORANGE  = [255, 128, 0]
    GREEN   = [0, 255, 0]
    PURPLE  = [255, 0, 255]