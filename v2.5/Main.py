import os
import copy
import torch
import shutil
import tempfile
import torchvision
import numpy                  as np
import torch.nn               as nn
import torch.optim            as optim
import torchvision.transforms as transforms

from NeuralNet                import Net, train

from math                     import floor

from kivy.app                 import App
from kivy.uix.widget          import Widget
from kivy.uix.boxlayout       import BoxLayout
from kivy.uix.image           import Image              as kiImage
from kivy.properties          import StringProperty
from kivy.config              import Config
from kivy.graphics.texture    import Texture
from kivy.clock               import Clock
from kivy.uix.popup           import Popup
from kivy.uix.label           import Label
from kivy.uix.screenmanager   import Screen, ScreenManager

from PIL                      import Image

Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '1162')
Config.set('graphics', 'height', '1020')

trainloader = None
testloader = None
net = None
imgs = None
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

ready = False


def setup():
    """
    Loads the datasets and DCNN model.
    In the case a model isn't saved in data, trains a new model.
    """
    global trainloader, testloader, net

    transform = transforms.Compose([transforms.ToTensor()])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=True, num_workers=2)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if os.path.isfile('./data/models/dcnn.pt'):
        print("Trained model found. Loading from saved state.")
        net = Net().to(device)
        net.load_state_dict(torch.load('./data/models/dcnn.pt'))
        net.eval()
    else:
        print("Beginning training of new model.")
        net = Net().to(device)

        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

        train(net, trainloader, device, optimizer, criterion, epochs=2)
   
        torch.save(net.state_dict(), './data/models/dcnn.pt')
        print("Model saved to /data/models/dcnn.pt")

def reset():
    """
    Loads a new image and label from the dataset and initializes a new window with them
    """
    global classes, imgs, testloader

    try:
        imgs, labels = iter(testloader).next()
    except Exception as e:
        print(str(e))
    img = (imgs[0,:,:,:]).numpy()
    img = np.moveaxis(img, 0, -1)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    imgs, labels = imgs.to(device), labels.to(device)
    outputs = net(imgs)
    _, predicted = torch.max(outputs, 1)
    label = classes[predicted[0]]

    if labels[0] == predicted[0]:
        print(outputs)
        return imgs, img, label
    else:
        return reset()

class GameImage(kiImage):
    shown_img = None
    shown_arr = None
    real_arr = None
    tensor = None

    remaining = None
    changes = None

    def retrieve_state(self):
        return self.tensor

    def init_game(self, imgs, img):
        global ready
        self.reset_image(imgs, img)
        App.get_running_app().time = 0
        ready=True

    def reset_image(self, imgs, img):
        self.remaining = 20
        App.get_running_app().remaining = "Remaining pixels:\n" + str(self.remaining)
        self.changes = list()

        self.tensor = copy.deepcopy(imgs)

        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.real_arr = (img*255).astype("uint8")
        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img

    def update_pixel(self, y, x):
        if (x, y) in self.changes:
            self.remaining += 1
            self.changes.remove((x, y))
        else:
            if self.remaining is 0:
                return
            self.remaining -= 1
            self.changes.append((x, y))

        # rgb = self.real_arr[y, x, :]
        rgb = [255, 255, 0]
        self.real_arr[y, x, :] = [255-rgb[0], 255-rgb[1], 255-rgb[2]]

        self.tensor[:,0,y,x] = (255-rgb[0])/255
        self.tensor[:,1,y,x] = (255-rgb[1])/255
        self.tensor[:,2,y,x] = (255-rgb[2])/255

        try:
            self.real_arr[y-1, x, :] = [255-rgb[0], 255-rgb[1], 255-rgb[2]]
            self.tensor[:,0,y-1,x] = (255-rgb[0])/255
            self.tensor[:,1,y-1,x] = (255-rgb[1])/255
            self.tensor[:,2,y-1,x] = (255-rgb[2])/255
        except Exception as e:
            pass

        try:
            self.real_arr[y+1, x, :] = [255-rgb[0], 255-rgb[1], 255-rgb[2]]
            self.tensor[:,0,y+1,x] = (255-rgb[0])/255
            self.tensor[:,1,y+1,x] = (255-rgb[1])/255
            self.tensor[:,2,y+1,x] = (255-rgb[2])/255
        except Exception as e:
            pass

        try:
            self.real_arr[y, x-1, :] = [255-rgb[0], 255-rgb[1], 255-rgb[2]]
            self.tensor[:,0,y,x-1] = (255-rgb[0])/255
            self.tensor[:,1,y,x-1] = (255-rgb[1])/255
            self.tensor[:,2,y,x-1] = (255-rgb[2])/255
        except Exception as e:
            pass

        try:
            self.real_arr[y, x+1, :] = [255-rgb[0], 255-rgb[1], 255-rgb[2]]
            self.tensor[:,0,y,x+1] = (255-rgb[0])/255
            self.tensor[:,1,y,x+1] = (255-rgb[1])/255
            self.tensor[:,2,y,x+1] = (255-rgb[2])/255
        except Exception as e:
            pass

        self.shown_arr = np.flipud(self.real_arr)
        self.shown_img = Texture.create(size=(32, 32), colorfmt="rgb")
        self.shown_img.blit_buffer(self.shown_arr.tostring(), bufferfmt="ubyte", colorfmt="rgb")
        self.texture = self.shown_img

        App.get_running_app().remaining = "Remaining pixels:\n" + str(self.remaining)
    
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            y = floor((self.height-(touch.y - self.y))/25)
            x = floor((touch.x - self.x)/25)
            print(x, y)
            print(self.shown_arr[y, x, :])
            self.update_pixel(y, x)


class GameApp(App):
    title = 'Net Breaker'
    imgs, img, label = None, None, None
    classification = StringProperty()
    remaining = StringProperty()
    time = 0
    time_label = StringProperty()
    score = 0
    score_label = StringProperty()

    def new_image(self):
        self.imgs, self.img, self.label = reset()
        self.classification = "Classification: " + str(self.label)
        self.root.ids.img.init_game(self.imgs, self.img)

    def evaluate(self):
        global classes, ready
        ready = False
        modified_tensor = self.root.ids.img.retrieve_state()

        print(np.array_equal(imgs.cpu().numpy(), modified_tensor.cpu().numpy()))
        
        with torch.no_grad():
            outputs = net(modified_tensor)
            _, predicted = torch.max(outputs, 1)

        if classes[predicted[0]]!= self.label:
            self.score += 1
            self.score_label = "Score:\n" + str(self.score)

        popup = Popup(title='Evaluation',
                      content=Label(text=("Predicted class of modified image: %s" % classes[predicted[0]])),
        size_hint=(None, None), size=(1000, 400))
        popup.open()

    def reset(self):
        self.root.ids.img.reset_image(self.imgs, self.img)

    def update_time(self, nap):
        global ready

        if ready:
            self.time += nap
            minutes, seconds = divmod(self.time, 60)
            self.root.ids.time.text = (
                '%02d:%02d.[size=20]%02d[/size]' %
                (int(minutes), int(seconds),
                int(seconds * 100 % 100))
            )

    def on_start(self):
        self.imgs, self.img, self.label = reset()
        self.classification = "Classification: " + str(self.label)
        self.remaining = "Remaining pixels:\n20"
        self.score_label = "Score:\n0"
        self.root.ids.img.init_game(self.imgs, self.img)
        Clock.schedule_interval(self.update_time, 0)


if __name__ == '__main__':
    setup()

    GameApp().run()